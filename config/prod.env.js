'use strict'
module.exports = {
  NODE_ENV: '"production"',
  theme: JSON.stringify(process.env.theme) || '"default"',
  flavour: JSON.stringify(process.env.flavour) || '"default"'
}
