// CSS Preloader: Inserts variable themes in Vue Components styles.

const path = require('path');

module.exports = function(source) {
  const isProduction = process.env.NODE_ENV === 'production'
  const variables = path.resolve(__dirname,
    `../src/themes/${process.env.theme}/variables.css`);

  return `${!isProduction ? `/* ${this.resource.split('/').pop()} */` : ''}
  @import "${variables}";
  ${source}`;
};

