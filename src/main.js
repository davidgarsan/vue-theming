// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: `<App theme="${process.env.theme}" flavour="${process.env.flavour}" />`,
  components: { App },
  beforeMount: function () {
    // this.theme = this.$el.attributes['data-theme'].value;console.log(this.$el);
  },
})
