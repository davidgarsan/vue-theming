import Vue from 'vue';
import Router from 'vue-router';
import StarzMenu from '@/components/StarzMenu';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'DemoStarz',
      component: StarzMenu
    }
  ]
});
